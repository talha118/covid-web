import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import SurveyForm from "../components/SurveyForm";

const useStyles = makeStyles(theme => ({
    container: {
        paddingLeft: theme.spacing(10),
        paddingRight: theme.spacing(10),
        paddingTop: theme.spacing(10),
    }
}));
export default function Home() {
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    return (
        <div className={classes.container}>
            <SurveyForm open={open} handleClose={()=>setOpen(false)}/>
        </div>
    );
}