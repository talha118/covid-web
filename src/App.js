import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Home from "./pages/Home";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#cc0033"
        },
        secondary: {
            main: "#5f6a72"
        }
    }
});

function App() {
    const [surveyResponseLast, setSurveyResponseLast] = useState(null);
    useEffect(() => {
        const last = localStorage.getItem('surveyResponseLast');
        if (last) {
            setSurveyResponseLast(new Date(last));
        }
    });
    return (
        // eslint-disable-next-line react/jsx-no-undef
        <ThemeProvider theme={{...theme, colorGray: "#5f6a72"}}>
            <Router>
                <Switch>
                    <Route
                        path={"/"}
                        component={Home}
                    />
                </Switch>
            </Router>
        </ThemeProvider>
    );
}

export default App;
