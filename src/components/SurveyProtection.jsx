import React, {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import {useTheme} from "@material-ui/core";
import Button from "@material-ui/core/Button";

const sliderOptions = [{value: 1, text: 1}, {value: 2, text: 2}, {value: 3, text: 3}, {value: 4, text: 4}, {
    value: 5,
    text: 5
}];
export default function SurveyProtection(props) {
    const [protectionLevel, setProtectionLevel] = useState(3);
    const [isSubmitted, setIsSubmitted] = useState(false);
    const theme = useTheme();

    const handleSubmit = ()=> {

    };
    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            <Typography variant={"h5"}>
                Select a protection level
            </Typography>
            <Typography variant={"body1"} style={{marginTop: theme.spacing(1)}}>We provide Default Privacy Guarantee for
                all.</Typography>
            <Typography variant={"body1"} style={{marginTop: theme.spacing(1)}}>For more details on privacy, click
                here</Typography>
            <Typography variant={"body1"} style={{marginTop: theme.spacing(1)}}>You can also change your privacy
                settings by moving a slider below.</Typography>
            <div style={{display: "flex", justifyContent: "center"}}>
                <div style={{
                    marginTop: theme.spacing(5),
                    padding: theme.spacing(2, 4),
                    backgroundColor: theme.palette.primary.main,
                    borderRadius: 30
                }}>
                    <Slider
                        color={"secondary"}
                        style={{width: 300}}
                        value={protectionLevel}
                        onChange={(e, v) => setProtectionLevel(v)}
                        valueLabelFormat={(value) => sliderOptions.findIndex((mark) => mark.value === value) + 1}
                        step={null}
                        valueLabelDisplay="auto"
                        marks={sliderOptions}
                        min={1}
                        max={5}
                    />
                </div>
            </div>
            <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(1)}}>
                <Button variant={"outlined"} color={"primary"} onClick={() => props.handleBack()} disabled={isSubmitted}>
                    Back
                </Button>
                <Button variant={"outlined"} style={{marginLeft: theme.spacing(3)}} color={"primary"} disabled={isSubmitted}
                        onClick={() => {
                            setIsSubmitted(true)
                            props.handleSubmit(protectionLevel)
                        }}>
                    Submit
                </Button>
            </div>
        </div>
    )
}