import React, {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import {useTheme} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import Alert from "./Alert";

export default function SurveyTested(props) {
    const [tested, setTested] = useState("");
    const [testResults, setTestResults] = useState("");
    const [testDate, setTestDate] = useState("");
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [error, setError] = useState("");
    const theme = useTheme();

    useEffect(()=>{
        setTested(props.tested);
        setTestResults(props.testResults);
        setTestDate(props.testDate);
    }, []);
    const handleNext = () => {
        if (!tested) {
            setError("Tested is required");
            setSnackbarOpen(true);
            return;
        }
        if (tested === "Yes") {
            if (!testResults || testDate === "") {
                setError("Results and test date are required");
                setSnackbarOpen(true);
                return;
            }
        }
        props.handleNext(tested, testResults, testDate);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setError("");
        setSnackbarOpen(false);
    };

    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            <Typography variant={"h5"}>
                Have you been tested for COVID-19?
            </Typography>
            <RadioGroup aria-label="tested" name="tested1" value={tested} onChange={(e) => setTested(e.target.value)}>
                <FormControlLabel value="Yes" control={<Radio/>} label="Positive"/>
                <FormControlLabel value="No" control={<Radio/>} label="Negative"/>
                <FormControlLabel value="Prefer not to say" control={<Radio/>} label="Prefer not to say"/>
            </RadioGroup>
            {tested === "Yes" && <React.Fragment>
                <Divider style={{margin: theme.spacing(1, 0)}}/>
                <Typography variant={"h5"}>Results: </Typography>
                <RadioGroup aria-label="testResults" name="testResults1" value={testResults}
                            onChange={(e) => setTestResults(e.target.value)}>
                    <FormControlLabel value="Yes" control={<Radio/>} label="Positive"/>
                    <FormControlLabel value="No" control={<Radio/>} label="Negative"/>
                    <FormControlLabel value="Prefer not to say" control={<Radio/>} label="Prefer not to say"/>
                </RadioGroup>
                <div>
                    <Typography variant={"h5"} style={{marginTop: theme.spacing(2)}}>When were you tested?</Typography>
                    <TextField
                        type={"date"}
                        value={testDate}
                        label={"Test Date"}
                        InputLabelProps={{shrink: true}}
                        variant={"outlined"}
                        margin={"normal"}
                        onChange={(e) => setTestDate(e.target.value)}
                        style={{minWidth: 200}}
                    />
                </div>
            </React.Fragment>}
            <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(1)}}>
                <Button variant={"outlined"} color={"primary"} onClick={() => props.setStep(0)}>
                    Back
                </Button>
                <Button variant={"outlined"} style={{marginLeft: theme.spacing(2)}} color={"primary"}
                        onClick={() => handleNext()}>
                    Next
                </Button>
            </div>
            <Snackbar open={snackbarOpen} onClose={handleClose} autoHideDuration={3000}>
                <Alert severity={"error"}>{error}</Alert>
            </Snackbar>
        </div>
    )
}