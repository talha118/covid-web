import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {useTheme} from "@material-ui/styles";
import Survey from "./Survery";
import DialogContent from "@material-ui/core/DialogContent";
import SurveyTested from "./SurveyTested";
import SurveyUnderlying from "./SurveyUnderlying";
import SurveyHousehold from "./SurveyHousehold";
import SurveyContact from "./SurveyContact";
import SurveyAboutYou from "./SurveyAboutYou";
import SurveyLocation from "./SurveyLocations";
import SurveyCounty from "./SurveyCounty";
import SurveyProtection from "./SurveyProtection";
import {http} from "../utils/http";
import {v1 as uuidv1} from 'uuid';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const symptomsMap = {
    cough: "Cough (new or worsening)",
    fever: "Fever",
    soarThroat: "Sore Throat",
    runnyNose: "Runny nose",
    shortnessOfBreath: "Shortness of breath or trouble breathing (new or worsening)",
    headache: "Headache",
    diarrhea: "Diarrhea",
    lossOfSenseOfSmell: "Loss of sense of smell",
    lossOfSenseOfTaste: "Loss of sense of taste",
    vomiting: "Nausea/Vomiting",
    muscleAches: "Muscle Aches",
    chills: "Chills",
    abdominalPain: "Abdominal pain"
};
const underlyingConditionsMap = {
    asthma: "Asthma",
    lungDisease: "Chronic Lung Disease",
    hyperTension: "Hypertension",
    diabetes: "Diabetes",
    heartDisease: "Heart Disease",
    obesity: "Obesity i.e. body-mass-index >30",
    immunocomprosied: "Immunocompromised (e.g. due to HIV/chemotherapy)",
}
export default function SurveyForm(props) {
    const theme = useTheme();
    const [step, setStep] = useState(0);
    const [symptomStartDate, setSymptomStartDate] = useState("");
    const [symptoms, setSymptoms] = useState({
        cough: false,
        fever: false,
        soarThroat: false,
        runnyNose: false,
        shortnessOfBreath: false,
        headache: false,
        diarrhea: false,
        lossOfSenseOfSmell: false,
        lossOfSenseOfTaste: false,
        vomiting: false,
        muscleAches: false,
        chills: false,
        abdominalPain: false
    });
    const [tested, setTested] = useState("");
    const [testResults, setTestResults] = useState("");
    const [testDate, setTestDate] = useState("");
    const [underlyingConditions, setUnderlyingConditions] = useState({
        asthma: false,
        lungDisease: false,
        hyperTension: false,
        diabetes: false,
        heartDisease: false,
        obesity: false,
        immunocomprosied: false,
    });
    const [householdCase, setHouseholdCase] = useState("");
    const [numberOfhouseholdPeople, setHouseholdPeople] = useState("");
    const [recentCovidInteraction, setRecentCovidInteraction] = useState("");
    const [recentCovidInteractionPositive, setRecentCovidInteractionPositive] = useState("");
    const [age, setAge] = useState("Under 18");
    const [gender, setGender] = useState("");
    const [genderValue, setGenderValue] = useState("");
    const [location, setLocation] = useState(null);
    const [county, setCounty] = useState(null);

    const preparePayload = (protectionLevel) => {
        const payload = {};
        payload.symptoms = [];
        Object.keys(symptoms).forEach(key => {
            if (symptoms[key]) {
                payload.symptoms.push(symptomsMap[key])
            }
        });
        if (Object.keys(symptoms).some(key => symptoms[key])) {
            payload.symptomStartDate = symptomStartDate;
        }
        payload.underlyingConditions = [];
        Object.keys(underlyingConditions).forEach(key => {
            if (underlyingConditions[key])
                payload.underlyingConditions.push(underlyingConditionsMap[key])
        });
        payload.tested = tested;
        if (payload.tested === "Yes") {
            payload.testResults = testResults;
            payload.testDate = testDate;
        }
        payload.householdCase = householdCase === "Yes" ? true : false;
        payload.numberOfhouseholdPeople = parseInt(numberOfhouseholdPeople);
        payload.recentCovidInteraction = recentCovidInteraction === "Yes" ? true : false;
        payload.recentCovidInteractionPositive = recentCovidInteractionPositive === "Yes" ? true : false;
        payload.age = age;
        if (gender === "Prefer to self describe as") {
            payload.gender = genderValue
        } else {
            payload.gender = gender;
        }
        if (location) {
            payload.location = {type: "Point", coordinates: [location.longitude, location.latitude]}
        } else {
            payload.county = `${county['Display Text']}`
        }
        payload.privacyLevel = protectionLevel;
        payload.deviceType = "web";
        return payload;

    };
    const handleSymptomsNext = (symptoms, symptomsStartDate) => {
        setSymptoms(symptoms);
        setSymptomStartDate(symptomsStartDate);
        setStep(1);
    };

    const handleTestedNext = (tested, testResults, testDate) => {
        setTested(tested);
        setTestResults(testResults);
        setTestDate(testDate);
        setStep(2);
    };

    const handleUnderlyingNext = (underlyingConditions) => {
        setUnderlyingConditions(underlyingConditions);
        setStep(3);
    };

    const handleHouseholdNext = (householdCase, numberOfHouseholdPeople) => {
        setHouseholdCase(householdCase);
        setHouseholdPeople(numberOfHouseholdPeople);
        setStep(4);
    };

    const handleContactNext = (recentCovidInteraction, recentCovidInteractionPositive) => {
        setRecentCovidInteraction(recentCovidInteraction);
        setRecentCovidInteractionPositive(recentCovidInteractionPositive);
        if(localStorage.getItem('lastTimeFormSubmitted')) {
            setStep(6);
        } else {
            setStep(5);
        }
    };

    const handleAboutYouNext = (age, gender, genderValue) => {
        setAge(age);
        setGender(gender);
        setGenderValue(genderValue);
        setStep(6);
    };

    const handleLocationNext = (location) => {
        setLocation(location);
        setStep(8);
    };

    const handleLocationSkip = () => {
        setLocation(null);
        setStep(7);
    };

    const handleCountyNext = (county) => {
        setCounty(county);
        setStep(8);
    };

    const handleProtectionBack = () => {
        if (location) {
            setStep(6);
        } else {
            setStep(7);
        }
    };

    const geocodePromise = (params) => {
        const geocoder = window.geoCoder;
        return new Promise(function (resolve, reject) {
            geocoder.geocode(params, function (results, status) {
                if (status !== "OK") {
                    reject()
                } else {
                    resolve(results)
                }
            })
        })
    };

    const resetState = () => {
        setStep(0);
        setSymptoms({
            cough: false,
            fever: false,
            soarThroat: false,
            runnyNose: false,
            shortnessOfBreath: false,
            headache: false,
            diarrhea: false,
            lossOfSenseOfSmell: false,
            lossOfSenseOfTaste: false,
            vomiting: false,
            muscleAches: false,
            chills: false,
            abdominalPain: false
        });
        setSymptomStartDate("");
        setTested("");
        setTestResults("");
        setTestDate("");
        setUnderlyingConditions({
            asthma: false,
            lungDisease: false,
            hyperTension: false,
            diabetes: false,
            heartDisease: false,
            obesity: false,
            immunocomprosied: false,
        });
        setHouseholdCase("");
        setHouseholdPeople("");
        setRecentCovidInteractionPositive("");
        setRecentCovidInteraction("");
        setAge("Under 18");
        setGender("");
        setGenderValue("");
        setLocation("");
        setCounty(null);
    };


    const handleFirstTimeSubmit = async (protectionLevel) => {
        try {
            const payload = preparePayload(protectionLevel);
            const location = await geocodePromise({'address': payload.county});
            payload.location = {
                type: 'Point',
                coordinates: [location[0].geometry.location.lng(), location[0].geometry.location.lat()]
            };
            payload.deviceId = uuidv1();
            await http.post("/survey-response/first-time", payload);
            localStorage.setItem('lastTimeSubmittedDate', new Date());
            props.handleClose();
            resetState();
        } catch (error) {
            alert("Something went wrong while submitting the response.");
            props.handleClose();
        }
    };

    return (
        <Dialog open={props.open} fullScreen={true}>
            <AppBar position={"relative"}>
                <Toolbar>
                    <Typography variant="h5">
                        Survey Form
                    </Typography>
                    <IconButton style={{marginLeft: "auto"}} onClick={()=>{
                        resetState();
                        props.handleClose();
                    }} color="inherit" aria-label="close">
                        <CloseIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <DialogContent
                style={{flexGrow: 1, padding: theme.spacing(10, 10, 0, 10), display: "flex", justifyContent: "center"}}>
                {step === 0 &&
                <Survey handleNext={handleSymptomsNext} symptoms={symptoms} symptomStartDate={symptomStartDate}/>}
                {step === 1 &&
                <SurveyTested handleNext={handleTestedNext} tested={tested} testResults={testResults}
                              testDate={testDate}
                              setStep={setStep}/>}
                {step === 2 &&
                <SurveyUnderlying handleNext={handleUnderlyingNext} underlyingConditions={underlyingConditions}
                                  setStep={setStep}/>}
                {step === 3 && <SurveyHousehold handleNext={handleHouseholdNext} householdCase={householdCase}
                                                numberOfhouseholdPeople={numberOfhouseholdPeople} setStep={setStep}/>}
                {step === 4 &&
                <SurveyContact handleNext={handleContactNext} recentCovidInteraction={recentCovidInteraction}
                               recentCovidInteractionPositive={recentCovidInteractionPositive} setStep={setStep}/>}
                {step === 5 &&
                <SurveyAboutYou handleNext={handleAboutYouNext} gender={gender} age={age} genderValue={genderValue}
                                setStep={setStep}/>}
                {step === 6 &&
                <SurveyLocation handleNext={handleLocationNext} location={location} handleSkip={handleLocationSkip}
                                setStep={setStep}/>}
                {step === 7 &&
                <SurveyCounty handleNext={handleCountyNext} county={county} setStep={setStep}/>}
                {step === 8 &&
                <SurveyProtection handleSubmit={handleFirstTimeSubmit} handleBack={handleProtectionBack}/>}
            </DialogContent>
        </Dialog>
    )
}