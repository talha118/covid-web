import React, {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {useTheme} from "@material-ui/styles";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "./Alert";

export default function Survey(props) {
    const [symptoms, setSymptoms] = useState({
        cough: false,
        fever: false,
        soarThroat: false,
        runnyNose: false,
        shortnessOfBreath: false,
        headache: false,
        diarrhea: false,
        lossOfSenseOfSmell: false,
        lossOfSenseOfTaste: false,
        vomiting: false,
        muscleAches: false,
        chills: false,
        abdominalPain: false
    });
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [symptomStartDate, setSymptomStartDate] = useState("");
    const [error, setError] = useState("");
    useEffect(()=>{
        setSymptoms(props.symptoms);
        setSymptomStartDate(props.symptomStartDate);
    }, []);
    const {cough, fever, soarThroat, runnyNose, headache, diarrhea, shortnessOfBreath, lossOfSenseOfSmell,
        lossOfSenseOfTaste, vomiting, muscleAches, chills, abdominalPain} = symptoms;
    const handleChange = (event) => {
        setSymptoms({...symptoms, [event.target.name]: event.target.checked})
    };
    const theme = useTheme();

    const handleNext = () => {
        if(Object.keys(symptoms).some(val => symptoms[val]) && symptomStartDate === "")  {
            setError("Symptom start date is required");
            setSnackbarOpen(true);
        } else {
            props.handleNext(symptoms, symptomStartDate);
        }
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setError("");
        setSnackbarOpen(false);
    };

    const today = new Date();
    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            <Typography variant={"h5"}>
                Do you currently have any of these COVID-19 symptoms?
            </Typography>
            <Typography variant={"subtitle2"}>
                Select all that apply
            </Typography>
            <FormGroup>
                <FormControlLabel
                    control={<Checkbox checked={fever} onChange={handleChange} name="fever"/>}
                    label="Fever"
                />
                <FormControlLabel
                    control={<Checkbox checked={cough} onChange={handleChange} name="cough"/>}
                    label="Cough (new or worsening)"
                />
                <FormControlLabel
                    control={<Checkbox checked={soarThroat} onChange={handleChange} name="soarThroat"/>}
                    label="Sore Throat"
                />
                <FormControlLabel
                    control={<Checkbox checked={runnyNose} onChange={handleChange} name="runnyNose"/>}
                    label="Runny nose"
                />
                <FormControlLabel
                    control={<Checkbox checked={shortnessOfBreath} onChange={handleChange} name="shortnessOfBreath"/>}
                    label="Shortness of breath or trouble breathing (new or worsening)"
                />
                <FormControlLabel
                    control={<Checkbox checked={headache} onChange={handleChange} name="headache"/>}
                    label="Headache"
                />
                <FormControlLabel
                    control={<Checkbox checked={diarrhea} onChange={handleChange} name="diarrhea"/>}
                    label="Diarrhea"
                />
                <FormControlLabel
                    control={<Checkbox checked={lossOfSenseOfSmell} onChange={handleChange} name="lossOfSenseOfSmell"/>}
                    label="Loss of sense of smell"
                />
                <FormControlLabel
                    control={<Checkbox checked={lossOfSenseOfTaste} onChange={handleChange} name="lossOfSenseOfTaste"/>}
                    label="Loss of sense of taste"
                />
                <FormControlLabel
                    control={<Checkbox checked={vomiting} onChange={handleChange} name="vomiting"/>}
                    label="Nausea/Vomiting"
                />
                <FormControlLabel
                    control={<Checkbox checked={muscleAches} onChange={handleChange} name="muscleAches"/>}
                    label="Muscle Aches"
                />
                <FormControlLabel
                    control={<Checkbox checked={chills} onChange={handleChange} name="chills"/>}
                    label="Chills"
                />
                <FormControlLabel
                    control={<Checkbox checked={abdominalPain} onChange={handleChange} name="abdominalPain"/>}
                    label="Abdominal Pain"
                />
                {Object.keys(symptoms).some(val=> symptoms[val]) && <div>
                    <Typography variant={"h5"}>
                        Approximately When did the symptoms start?
                    </Typography>
                    <TextField
                        value={symptomStartDate}
                        label={"Date"}
                        type={"date"}
                        variant={"outlined"}
                        margin={"normal"}
                        style={{minWidth: 200}}
                        InputLabelProps={{ shrink: true }}
                        onChange={(event)=> {setSymptomStartDate(event.target.value)}}
                    />
                </div>}
                <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(1), marginBottom: theme.spacing(10)}}>
                    <Button variant={"outlined"} color={"primary"} onClick={handleNext}>
                        Next
                    </Button>
                </div>
                <Snackbar open={snackbarOpen} onClose={handleClose} autoHideDuration={3000}>
                    <Alert severity={"error"}>{error}</Alert>
                </Snackbar>
            </FormGroup>
        </div>
    )
}