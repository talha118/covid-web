import React, {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import RadioGroup from "@material-ui/core/RadioGroup/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import {useTheme} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import Alert from "./Alert";

export default function SurveyContact(props) {
    const [recentCovidInteraction, setRecentCovidInteraction] = useState("");
    const [recentCovidInteractionPositive, setRecentCovidInteractionPositive] = useState("");
    const [error, setError] = useState("");
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    useEffect(() => {
        setRecentCovidInteraction(props.recentCovidInteraction);
        setRecentCovidInteractionPositive(props.recentCovidInteractionPositive);
    }, []);
    const theme = useTheme();
    const handleNext = () => {
        if (recentCovidInteraction === "" || recentCovidInteractionPositive === "") {
            setError("Information about your interaction with any COVID patient is required");
            setSnackbarOpen(true);
            return;
        }
        props.handleNext(recentCovidInteraction, recentCovidInteractionPositive);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setError("");
        setSnackbarOpen(false);
    };

    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            <Typography variant={"h5"}>
                In the past 14 days, have you had any contact with someone who tested positive for COVID-19?
            </Typography>
            <RadioGroup aria-label="recentCovidInteraction" name="recentCovidInteraction1"
                        value={recentCovidInteraction}
                        onChange={(e) => setRecentCovidInteraction(e.target.value)}>
                <FormControlLabel value="Yes" control={<Radio/>} label="Yes"/>
                <FormControlLabel value="No" control={<Radio/>} label="No"/>
            </RadioGroup>
            <Typography variant={"h5"} style={{marginTop: theme.spacing(2)}}>
                Did you come across anyone with COVID-19 symptoms, in the past 14 days?
            </Typography>
            <RadioGroup aria-label="recentCovidInteractionPositive" name="recentCovidInteractionPositive1"
                        value={recentCovidInteractionPositive}
                        onChange={(e) => setRecentCovidInteractionPositive(e.target.value)}>
                <FormControlLabel value="Yes" control={<Radio/>} label="Yes"/>
                <FormControlLabel value="No" control={<Radio/>} label="No"/>
            </RadioGroup>
            <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(1)}}>
                <Button variant={"outlined"} color={"primary"} onClick={() => props.setStep(3)}>
                    Back
                </Button>
                <Button variant={"outlined"} color={"primary"} style={{marginLeft: theme.spacing(3)}}
                        onClick={handleNext}>
                    Next
                </Button>
            </div>
            <Snackbar open={snackbarOpen} onClose={handleClose} autoHideDuration={3000}>
                <Alert severity={"error"}>{error}</Alert>
            </Snackbar>
        </div>
    )
}