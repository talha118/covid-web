import React, {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";
import useTheme from "@material-ui/core/styles/useTheme";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import countyOptions from "../media/data/county names";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ListSubheader from '@material-ui/core/ListSubheader';
import {VariableSizeList} from "react-window";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import Alert from "./Alert";

const LISTBOX_PADDING = 8; // px

function renderRow(props) {
    const {data, index, style} = props;
    return React.cloneElement(data[index], {
        style: {
            ...style,
            top: style.top + LISTBOX_PADDING,
        },
    });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
    const outerProps = React.useContext(OuterElementContext);
    return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
    const ref = React.useRef(null);
    React.useEffect(() => {
        if (ref.current != null) {
            ref.current.resetAfterIndex(0, true);
        }
    }, [data]);
    return ref;
}

const ListboxComponent = React.forwardRef(function ListboxComponent(props, ref) {
    const {children, ...other} = props;
    const itemData = React.Children.toArray(children);
    const theme = useTheme();
    const smUp = useMediaQuery(theme.breakpoints.up('sm'), {noSsr: true});
    const itemCount = itemData.length;
    const itemSize = smUp ? 36 : 48;

    const getChildSize = (child) => {
        if (React.isValidElement(child) && child.type === ListSubheader) {
            return 48;
        }

        return itemSize;
    };

    const getHeight = () => {
        if (itemCount > 8) {
            return 8 * itemSize;
        }
        return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
    };

    const gridRef = useResetCache(itemCount);

    return (
        <div ref={ref}>
            <OuterElementContext.Provider value={other}>
                <VariableSizeList
                    itemData={itemData}
                    height={getHeight() + 2 * LISTBOX_PADDING}
                    width="100%"
                    ref={gridRef}
                    outerElementType={OuterElementType}
                    innerElementType="ul"
                    itemSize={(index) => getChildSize(itemData[index])}
                    overscanCount={5}
                    itemCount={itemCount}
                >
                    {renderRow}
                </VariableSizeList>
            </OuterElementContext.Provider>
        </div>
    );
});

export default function SurveyCounty(props) {
    const [county, setCounty] = useState(null);
    const [error, setError] = useState("");
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    useEffect(()=> {
        setCounty(props.county);
    }, []);
    const theme = useTheme();

    const handleNext = () => {
        if (!county) {
            setError("Please select a county");
            setSnackbarOpen(true);
            return;
        }
        props.handleNext(county);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setError("");
        setSnackbarOpen(false);
    };

    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            <Typography variant={"h4"} style={{color: theme.colorGray, fontWeight: 500}}> LOCATION</Typography>
            <Typography variant={"body1"} style={{marginTop: theme.spacing(1)}}>Please enter your county
                instead</Typography>
            <div style={{marginTop: theme.spacing(5)}}>
                <Typography variant={"h5"}>County</Typography>
                <Autocomplete
                    options={countyOptions}
                    getOptionLabel={option => `${option['Display Text']}`}
                    value={county}
                    ListboxComponent={ListboxComponent}
                    onChange={(e, v) => {
                        setCounty(v)
                    }}
                    renderInput={(params) => <TextField {...params} style={{minWidth: 300}} margin="normal"
                                                        variant="outlined"/>}
                />
            </div>
            <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(3)}}>
                <Button variant={"outlined"} color={"primary"} onClick={() => props.setStep(5)}>
                    Back
                </Button>
                {!localStorage.getItem("lastTimeSubmittedDate")?
                <Button variant={"outlined"} color={"primary"} style={{marginLeft: theme.spacing(3)}}
                        onClick={()=> handleNext()}>
                    Next
                </Button>
                    :
                    <Button variant={"contained"} color={"primary"} style={{marginLeft: theme.spacing(3)}}>
                    Submit
                    </Button>
                }
            </div>
            <Snackbar open={snackbarOpen} onClose={handleClose} autoHideDuration={3000}>
                <Alert severity={"error"}>{error}</Alert>
            </Snackbar>
        </div>
    )
}