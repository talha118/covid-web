import React, {useEffect, useState} from "react";
import useTheme from "@material-ui/core/styles/useTheme";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import Alert from "./Alert";

export default function SurveyLocation(props) {
    const [error, setError] = useState("");
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [location, setLocation] = useState(null);
    const theme = useTheme();
    useEffect(() => setLocation(props.location), []);
    const getLocation = () => {
        navigator.permissions.query({name: 'geolocation'}).then(function (result) {
            if (result.state == 'granted') {
                navigator.geolocation.getCurrentPosition((data) => {
                    setLocation({latitude: data.coords.latitude, longitude: data.coords.longitude})
                })
            } else if (result.state == 'prompt') {
                navigator.geolocation.getCurrentPosition((data) => {
                    setLocation({latitude: data.coords.latitude, longitude: data.coords.longitude});
                }, () => {
                    setError("Location access is denied. Please go to the browser settings and allow location.");
                    setSnackbarOpen(true);
                })
            } else if (result.state == 'denied') {
                setError("Location access is denied. Please go to the browser settings and allow location.");
                setSnackbarOpen(true);
            }
        })
    };

    const handleNext = () => {
        if (!location) {
            setError("Please skip if you do not wish to provide your location");
            setSnackbarOpen(true);
            return;
        }
        props.handleNext(location);

    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    return (
        <div style={{display: "flex", flexDirection: "column", paddingBottom: theme.spacing(10)}}>
            <Typography variant={"h4"} style={{color: theme.colorGray}}>
                Location
            </Typography>
            <Typography variant={"body1"} style={{marginTop: theme.spacing(1)}}>
                To monitor the virus in your area we need your location. We use your location for research only, as
                described here.
            </Typography>
            <div style={{display: "flex", marginTop: theme.spacing(4), justifyContent: "center"}}>
                <Button
                    variant={"contained"}
                    color={"primary"}
                    style={{
                        padding: theme.spacing(1, 5),
                        // backgroundColor: theme.palette.primary.main,
                        borderRadius: 30,
                        color: "white",
                        textTransform: "none"
                    }}
                    onClick={() => getLocation()}
                >
                    Get Location
                </Button>
            </div>
            <div style={{display: "flex", justifyContent: "flex-end", marginTop: theme.spacing(2)}}>
                <Button color={"primary"} onClick={() => props.handleSkip()}>
                    Enter Location Manually
                </Button>
                <Button variant={"outlined"} color={"primary"} style={{marginLeft: theme.spacing(3)}}
                        onClick={() => props.setStep(5)}>
                    Back
                </Button>
                {!localStorage.getItem("lastTimeSubmittedDate") ?
                <Button variant={"outlined"} color={"primary"} style={{marginLeft: theme.spacing(3)}}
                        onClick={handleNext}>
                    Next
                </Button>
                    :
                    <Button variant={"contained"} color={"primary"} style={{marginLeft: theme.spacing(3)}}>
                        Submit
                    </Button>
                }
            </div>
            <Snackbar open={snackbarOpen} onClose={handleClose} autoHideDuration={3000}>
                <Alert severity={"error"}>{error}</Alert>
            </Snackbar>
        </div>
    )
}